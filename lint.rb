# Commande de linting
lint_command = "rubocop"

# Exécute la commande de linting et récupère le code de sortie
lint_exit_code = system(lint_command)

# Vérifie le code de sortie
if lint_exit_code
  puts "Le linting a réussi. Vous pouvez effectuer le commit."
else
  puts "Le linting a échoué. Corrigez les erreurs avant de commettre."
  exit 1
end
