# Utilisez une image de base Ruby avec la version souhaitée
FROM ruby:3.1.2

# Répertoire de travail dans le conteneur
WORKDIR /app

# Mettez à jour RubyGems vers une version compatible
RUN gem update --system

# Installez Bundler 2.4.18
RUN gem install bundler:2.4.18
RUN apt-get update -qq && apt-get install -y -qq apt-transport-https ca-certificates curl software-properties-common



# Installez les dépendances Ruby avec Bundler
COPY Gemfile Gemfile.lock ./
RUN gem install rubocop
RUN bundle install
RUN rails db:create
RUN rails db:migrate

# Installez un moteur JavaScript (Node.js)
RUN apt-get update && apt-get install -y nodejs

# Copiez tout le reste de votre application dans le conteneur
COPY . .

EXPOSE 3000

# Commande pour démarrer le serveur Rails
CMD ["rails", "s", "0.0.0.0"]
