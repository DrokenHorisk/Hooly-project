# Commande pour exécuter les tests unitaires
require 'English'
tests_command = "rspec"

# Exécute la commande des tests unitaires
system(tests_command)

# Vérifie le code de sortie de la commande des tests
if $CHILD_STATUS.success?
  puts "Les tests unitaires ont réussi. Vous pouvez effectuer le push."
else
  puts "Les tests unitaires ont échoué. Corrigez les erreurs avant de pousser."
  exit 1
end
