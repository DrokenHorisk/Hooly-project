#!/bin/bash

# Chemin vers le répertoire du projet
PROJECT_DIR="$(pwd)"

# Nom du hook pre-commit
PRE_COMMIT_HOOK="pre-commit"

# Nom du hook pre-push
PRE_PUSH_HOOK="pre-push"

# Chemin vers le script Ruby de linting
LINT_SCRIPT="lint.rb"

# Chemin vers le script Ruby de tests unitaires
TESTS_SCRIPT="tests.rb"

# Copier le hook pre-commit
echo -e "#!/bin/bash\nruby $LINT_SCRIPT\n" > "$PROJECT_DIR/.git/hooks/$PRE_COMMIT_HOOK"

# Copier le hook pre-push
echo -e "#!/bin/bash\nruby $TESTS_SCRIPT\n" > "$PROJECT_DIR/.git/hooks/$PRE_PUSH_HOOK"

# Rendre les hooks exécutables
chmod +x "$PROJECT_DIR/.git/hooks/$PRE_COMMIT_HOOK"
chmod +x "$PROJECT_DIR/.git/hooks/$PRE_PUSH_HOOK"

echo "Hooks pre-commit et pre-push installés avec succès."
