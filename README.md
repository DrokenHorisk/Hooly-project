
# Projet de groupe
Création d'un site de réservation d'emplacement de Food truck


## Auteurs

- [@Benjamin](https://gitlab.com/DrokenHorisk)
- [@Alexandre](https://gitlab.com/yunghope)

## DEMO
Add a gif here
## Documentation

### Sujet du projet
Le client Hooly loue des emplacements de parking pour des foodtrucks. Il a besoin d’un programme lui permettant de gérer les réservations.
Voici le cahier des charges :
- Hooly possède 7 emplacements de parking chaque jour de la semaine, sauf le vendredi où il n’en possède que 6.
- Chaque foodtruck ne peut réserver une place qu’une fois par semaine
- Un emplacement ne peut être alloué qu’à un seul foodtruck par jour
- Il est impossible de réserver pour une date passée et le jour même
### Fonctionnalités :
- Réserver un emplacement
- Lister les emplacements
- Supprimer une réservation

- Interface admin (connexion + back office)
- Création des emplacements
- Connexion utilisateur
## Déroulement du TP
Le but du TP est de mettre en application les cours de gestion de version (Git) et CI-CD. Dans le cadre de ce TP,par groupe de 2 et travailler ensemble sur un même dépôt.

Les choix des technos est libre cependant vous devez faire une application web (monolithe
ou application web + api).
Organisation
- Choix de la stratégie de gestion des branches
- Mise en place du dépôt en commun (en fonction de la stratégie choisi)
- Réalisation de la mission (chacun une fonctionnalité)

## Objectifs
- Mise en place d’un dépôt Git
- Création d’un dépôt Git distant (GitLab.com)
- Application des conventions de nommage et workflow
- Utilisation de branches
- Résolution de conflits de merge
